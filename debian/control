Source: goawk
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Guilherme Puida Moreira <guilherme@puida.xyz>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-golang,
               golang-any,
               gawk <!nocheck>,
               procps <!nocheck>,
               scdoc <!nodoc>,
Testsuite: autopkgtest-pkg-go
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/goawk
Vcs-Git: https://salsa.debian.org/go-team/packages/goawk.git
Homepage: https://github.com/benhoyt/goawk
XS-Go-Import-Path: github.com/benhoyt/goawk

Package: goawk
Section: interpreters
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends}
Static-Built-Using: ${misc:Static-Built-Using}
Description: POSIX-compliant AWK interpreter written in Go, with CSV support (program)
 GoAWK is a POSIX-compatible version of AWK, and additionally has a CSV
 mode for reading and writing CSV and TSV files.
 .
 Additional features GoAWK has over AWK:
 .
  * It has proper support for CSV and TSV files.
  * It's the only AWK implementation with a code coverage feature.
  * It supports negative field indexes to access fields from the right,
    for example, $-1 refers to the last field.
  * It's embeddable in your Go programs! You can even call custom Go
    functions from your AWK scripts.
  * Most AWK scripts are faster than awk and on a par with gawk, though
    usually slower than mawk.
  * The parser supports 'single-quoted strings' in addition to "double-
    quoted strings", primarily to make Windows one-liners easier when using
    the cmd.exe shell (which uses " as the quote character).
 .
 This package provides the CLI interface to goawk.

Package: golang-github-benhoyt-goawk-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: POSIX-compliant AWK interpreter written in Go, with CSV support (library)
 GoAWK is a POSIX-compatible version of AWK, and additionally has a CSV
 mode for reading and writing CSV and TSV files.
 .
 Additional features GoAWK has over AWK:
 .
  * It has proper support for CSV and TSV files.
  * It's the only AWK implementation with a code coverage feature.
  * It supports negative field indexes to access fields from the right,
    for example, $-1 refers to the last field.
  * It's embeddable in your Go programs! You can even call custom Go
    functions from your AWK scripts.
  * Most AWK scripts are faster than awk and on a par with gawk, though
    usually slower than mawk.
  * The parser supports 'single-quoted strings' in addition to "double-
    quoted strings", primarily to make Windows one-liners easier when using
    the cmd.exe shell (which uses " as the quote character).
 .
 This package provides the goawk library.
